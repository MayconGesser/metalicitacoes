import scrapy
from scrapy import Request
import urllib

class QuotesSpider(scrapy.Spider):
	name = "quotes"

	def start_requests(self):
		urls = ['https://www.licitacoes-e.com.br/aop/pesquisar-licitacao.aop?opcao=preencherPesquisar']
		
		#parte da pagina dinamica, necessario emular browser		
		curl_cmd = "curl 'https://www.licitacoes-e.com.br/aop/pesquisar-licitacao.aop' -H 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:81.0) Gecko/20100101 Firefox/81.0' -H 'Accept: */*' -H 'Accept-Language: en-US,en;q=0.5' --compressed -H 'Content-Type: application/x-www-form-urlencoded; charset=UTF-8' -H 'X-Requested-With: XMLHttpRequest' -H 'Origin: https://www.licitacoes-e.com.br' -H 'DNT: 1' -H 'Connection: keep-alive' -H 'Referer: https://www.licitacoes-e.com.br/aop/pesquisar-licitacao.aop?opcao=preencherPesquisar' -H 'Cookie: JSESSIONID=uk0fdAY_VNHg0Krn-XAol5jqHSaX09xOmVSMURudJ0ZoVIRFR7oa!-515136286!NONE' -H 'Cache-Control: max-age=0' --data-raw '&opcao=preencherPesquisarSituacao&numeroLicitacao=&numeroEdital=&codigoModalidade=&codigoComprador=&codigoSituacao=&textoSiglaUnidadeFederativa=&codigoPeriodo=&textoMercadoria=&participacao=&tratamento=&bird=&bid='"
		request = Request.from_curl(curl_cmd)
		yield request		

	def parse(self, response):
		captcha_src = response.css('#img_captcha').xpath('@src').get()
		f = open('captcha.png', 'wb')
		f.write(urllib.request.urlopen(captcha_src).read())
		f.close()
		"""return scrapy.FormRequest.from_response(
			response,
			method="POST",
			formdata={'select0' : '2'},
			callback=self.log_response
		)"""
		
	def log_response(self, response):
		filename = 'outracoisa.html'
		with open(filename, 'wb') as f:
			f.write(response.body)
		self.log(f'Saved file {filename}')
